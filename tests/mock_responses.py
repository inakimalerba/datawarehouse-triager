# flake8: noqa: E501
# pylint: disable-all
pipeline_517056 = {
    "commit_id": "e5f1f51a2ad39c98ac1be8a09fe5844373d38280",
    "commit_message_title": "net: macb: Fix handling of fixed-link node",
    "project": {
        "project_id": 2,
        "path": "cki-project/cki-pipeline"
    },
    "gittree": {
        "name": "upstream-stable"
    },
    "web_url": "https://gitlab/cki-project/cki-pipeline/pipelines/517056",
    "pipeline_id": 517056,
    "created_at": "2020-04-02T20:33:11.765000Z",
    "started_at": "2020-04-02T20:33:13.731000Z",
    "finished_at": "2020-04-03T06:04:49.347000Z",
    "duration": 34286,
    "test_hash": "21170b2c79869183873a8213bac490a543b3cc68",
    "tag": "-21170b2",
    "kernel_version": "5.5.15-21170b2.cki",
    "test_passed": False,
    "build_passed": True,
    "merge_passed": True,
    "lint_passed": True,
    "test_passed_status": "ERROR",
    "build_passed_status": "PASS",
    "merge_passed_status": "PASS",
    "lint_passed_status": "SKIP",
    "all_passed": False,
    "all_passed_status": "FAIL",
    "trigger_variables": {
        "arch_override": "aarch64 ppc64le x86_64 s390x",
        "branch": "queue/5.5",
        "builder_image": "registry.gitlab.com/cki-project/containers/builder-fedora",
        "cki_pipeline_branch": "upstream-stable",
        "cki_pipeline_id": "9ae2ad9c63a81e1f7ece099db65c33a6c378499c4f94f4b50a73c596f37a35d7",
        "cki_pipeline_type": "baseline",
        "cki_project": "cki-project/cki-pipeline",
        "commit_hash": "21170b2c79869183873a8213bac490a543b3cc68",
        "config_target": "olddefconfig",
        "git_url": "https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable-rc.git",
        "kernel_type": "upstream",
        "mail_add_maintainers_to": "cc",
        "mail_bcc": "email@redhat.com",
        "mail_from": "CKI Project <cki-project@redhat.com>",
        "mail_to": "Linux Stable maillist <stable@vger.kernel.org>",
        "make_target": "targz-pkg",
        "name": "stable-queue",
        "report_template": "limited",
        "report_types": "email",
        "require_manual_review": "True",
        "title": "Baseline: stable-queue queue/5.5:21170b2c7986",
        "tree_name": "upstream"
    }
}

issuerecord_57 = {
    "id": 2721,
    "pipeline_id": 517056,
    "issue": {
        "id": 57,
        "kind": {
            "id": 4,
            "description": "Unidentified",
            "tag": "Unidentified"
        },
        "description": "Unidentified Failures - Need manual intervention.",
        "ticket_url": "https://not-an.url",
        "resolved": False,
        "generic": False
    },
    "testruns": [
        {
            "id": 791303,
            "test": {
                "id": 1,
                "name": "Boot test",
                "fetch_url": "https://github.com/CKI-project/tests-beaker/archive/master.zip#distribution/kpkginstall",
                "maintainers": [],
                "universal_id": "boot"
            },
            "waived": False,
            "kernel_arch": {
                "name": "ppc64le"
            },
            "kernel_debug": False,
            "logs": [
                {
                    "name": "ppc64le_3_console.log",
                    "file": "https://cki-artifacts.s3.amazonaws.com/datawarehouse/2020/04/02/517056/ppc64le_3_console.log"
                }
            ],
            "invalid_result": False,
            "untrusted": False,
            "targeted": False,
            "trace_url": "https://gitlab/api/v4/projects/2/jobs/752962/trace",
            "result": {
                "name": "ERROR"
            },
            "beaker_result": {
                "name": "Warn"
            },
            "beaker_status": {
                "name": "Aborted"
            },
            "beaker_resource": {
                "id": 159,
                "fqdn": "machine.at.beaker"
            },
            "retcode": 2,
            "task_id": 108484383,
            "recipe_id": 8091560,
            "recipe_url": "https://beaker/recipes/8091560",
            "duration": 34
        },
    ],
    "bot_generated": True
}

issuerecord_36 = {
    "id": 2720,
    "pipeline_id": 517056,
    "issue": {
        "id": 36,
        "kind": {
            "id": 2,
            "description": "Infrastructure problem",
            "tag": "Infra"
        },
        "description": "Test Timeout",
        "ticket_url": "https://beaker/recipes/7728874#task104120040",
        "resolved": False,
        "generic": False
    },
    "testruns": [
        {
            "id": 791584,
            "test": {
                "id": 39,
                "name": "Podman system integration test - as root",
                "fetch_url": "https://github.com/CKI-project/tests-beaker/archive/master.zip#container/podman",
                "maintainers": [
                ],
                "universal_id": "podman.system"
            },
            "waived": False,
            "kernel_arch": {
                "name": "s390x"
            },
            "kernel_debug": False,
            "logs": [
                {
                    "name": "Podman_system_integration_test___as_root/s390x_2_podman.system_taskout.log",
                    "file": "https://cki-artifacts.s3.amazonaws.com/datawarehouse/2020/04/02/517056/Podman_system_integration_test___as_root/s390x_2_podman.system_taskout.log"
                },
                {
                    "name": "Podman_system_integration_test___as_root/s390x_2_podman.system_resultoutputfile.log",
                    "file": "https://cki-artifacts.s3.amazonaws.com/datawarehouse/2020/04/02/517056/Podman_system_integration_test___as_root/s390x_2_podman.system_resulto_0KbA26G.log"
                },
                {
                    "name": "s390x_2_console.log",
                    "file": "https://cki-artifacts.s3.amazonaws.com/datawarehouse/2020/04/02/517056/s390x_2_console.log"
                }
            ],
            "invalid_result": False,
            "untrusted": False,
            "targeted": False,
            "trace_url": "https://gitlab/api/v4/projects/2/jobs/752963/trace",
            "result": {
                "name": "ERROR"
            },
            "beaker_result": {
                "name": "Warn"
            },
            "beaker_status": {
                "name": "Aborted"
            },
            "beaker_resource": {
                "id": 597,
                "fqdn": "machine.beaker"
            },
            "retcode": 2,
            "task_id": 108484293,
            "recipe_id": 8091556,
            "recipe_url": "https://beaker/recipes/8091556",
            "duration": 741
        }
    ],
    "bot_generated": True
}

pipeline_517056_issuerecords = {
    "count": 2,
    "next": None,
    "previous": None,
    "results": {
        "issue_records": [
            issuerecord_36,
            issuerecord_57,
        ]
    }
}

pipeline_517056_issuerecords_57 = {
    "count": 1,
    "next": None,
    "previous": None,
    "results": {
        "issue_records": [
            issuerecord_57,
        ]
    }
}

pipeline_517056_failed_tests = {
    "count": 2,
    "next": None,
    "previous": None,
    "results": {
        "jobs": [
            {
                "id": 791303,
                "test": {
                    "id": 1,
                    "name": "Boot test",
                    "fetch_url": "https://github.com/CKI-project/tests-beaker/archive/master.zip#distribution/kpkginstall",
                    "maintainers": [],
                    "universal_id": "boot"
                },
                "waived": False,
                "kernel_arch": {
                    "name": "ppc64le"
                },
                "kernel_debug": False,
                "logs": [
                    {
                        "name": "ppc64le_3_console.log",
                        "file": "https://cki-artifacts.s3.amazonaws.com/datawarehouse/2020/04/02/517056/ppc64le_3_console.log"
                    }
                ],
                "invalid_result": False,
                "untrusted": False,
                "targeted": False,
                "trace_url": "https://gitlab/api/v4/projects/2/jobs/752962/trace",
                "result": {
                    "name": "ERROR"
                },
                "beaker_result": {
                    "name": "Warn"
                },
                "beaker_status": {
                    "name": "Aborted"
                },
                "beaker_resource": {
                    "id": 159,
                    "fqdn": "machine.beaker"
                },
                "retcode": 2,
                "task_id": 108484383,
                "recipe_id": 8091560,
                "recipe_url": "https://beaker/recipes/8091560",
                "duration": 34
            },
            {
                "id": 791256,
                "test": {
                    "id": 1,
                    "name": "Boot test",
                    "fetch_url": "https://github.com/CKI-project/tests-beaker/archive/master.zip#distribution/kpkginstall",
                    "maintainers": [],
                    "universal_id": "boot"
                },
                "waived": False,
                "kernel_arch": {
                    "name": "ppc64le"
                },
                "kernel_debug": False,
                "logs": [
                    {
                        "name": "ppc64le_1_console.log",
                        "file": "https://cki-artifacts.s3.amazonaws.com/datawarehouse/2020/04/02/517056/ppc64le_1_console.log"
                    }
                ],
                "invalid_result": False,
                "untrusted": False,
                "targeted": False,
                "trace_url": "https://gitlab/api/v4/projects/2/jobs/752962/trace",
                "result": {
                    "name": "ERROR"
                },
                "beaker_result": {
                    "name": "Warn"
                },
                "beaker_status": {
                    "name": "Aborted"
                },
                "beaker_resource": {
                    "id": 585,
                    "fqdn": "machine.beaker"
                },
                "retcode": 2,
                "task_id": 108484341,
                "recipe_id": 8091559,
                "recipe_url": "https://beaker/recipes/8091559",
                "duration": 34
            },
            {
                "id": 791584,
                "test": {
                    "id": 39,
                    "name": "Podman system integration test - as root",
                    "fetch_url": "https://github.com/CKI-project/tests-beaker/archive/master.zip#container/podman",
                    "maintainers": [
                    ],
                    "universal_id": "podman.system"
                },
                "waived": False,
                "kernel_arch": {
                    "name": "s390x"
                },
                "kernel_debug": False,
                "logs": [
                    {
                        "name": "Podman_system_integration_test___as_root/s390x_2_podman.system_taskout.log",
                        "file": "https://cki-artifacts.s3.amazonaws.com/datawarehouse/2020/04/02/517056/Podman_system_integration_test___as_root/s390x_2_podman.system_taskout.log"
                    },
                    {
                        "name": "Podman_system_integration_test___as_root/s390x_2_podman.system_resultoutputfile.log",
                        "file": "https://cki-artifacts.s3.amazonaws.com/datawarehouse/2020/04/02/517056/Podman_system_integration_test___as_root/s390x_2_podman.system_resulto_0KbA26G.log"
                    },
                    {
                        "name": "s390x_2_console.log",
                        "file": "https://cki-artifacts.s3.amazonaws.com/datawarehouse/2020/04/02/517056/s390x_2_console.log"
                    }
                ],
                "invalid_result": False,
                "untrusted": False,
                "targeted": False,
                "trace_url": "https://gitlab/api/v4/projects/2/jobs/752963/trace",
                "result": {
                    "name": "ERROR"
                },
                "beaker_result": {
                    "name": "Warn"
                },
                "beaker_status": {
                    "name": "Aborted"
                },
                "beaker_resource": {
                    "id": 597,
                    "fqdn": "machine.beaker"
                },
                "retcode": 2,
                "task_id": 108484293,
                "recipe_id": 8091556,
                "recipe_url": "https://beaker/recipes/8091556",
                "duration": 741
            }
        ]
    }
}

lookup_issue_88 = {
    "id": 2,
    "issue": {
        "id": 88,
        "kind": {
            "id": 1,
            "description": "Kernel bug",
            "tag": "Kernel Bug"
        },
        "description": "Bug description",
        "ticket_url": "https://bug.link",
        "resolved": False,
        "generic": False
    },
    "text_match": "bnx2x .* Direct firmware load for",
    "file_name_match": None,
    "test_name_match": None
}
