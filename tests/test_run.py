"""Test run.py."""
import os
from dataclasses import dataclass
import unittest
from mock import patch, call, Mock, ANY
import responses
from tests import mock_responses

os.environ['DATAWAREHOUSE_URL'] = 'http://server'
import run  # noqa: E402 # pylint: disable=wrong-import-position


class TestRun(unittest.TestCase):
    """TestRun."""

    @responses.activate
    def setUp(self):
        """SetUp."""
        responses.add(responses.GET, 'http://server/api/1/pipeline/517056', json=mock_responses.pipeline_517056)
        self.pipeline = run.Pipeline(517056)

    @responses.activate
    def test_get_issue_record_empty(self):
        """Test get_issue_record. No records for issue_id."""
        responses.add(responses.GET, 'http://server/api/1/pipeline/517056/issue/record?issue_id=1',
                      json={'results': {'issue_records': []}})
        self.assertEqual(None, self.pipeline.get_issue_record(1))

    @responses.activate
    def test_get_issue_record(self):
        """Test get_issue_record."""
        responses.add(responses.GET, 'http://server/api/1/pipeline/517056/issue/record?issue_id=57',
                      json=mock_responses.pipeline_517056_issuerecords_57)
        issue_record = self.pipeline.get_issue_record(57)
        self.assertIsNotNone(issue_record)

    @responses.activate
    def test_get_issue_record_empty_multiple(self):
        """Test get_issue_record. Multiple records raise."""
        responses.add(
            responses.GET, 'http://server/api/1/pipeline/517056/issue/record?issue_id=1',
            json={
                'results': {
                    'issue_records': [
                        mock_responses.issuerecord_57,
                        mock_responses.issuerecord_57,
                    ]
                }
            }
        )
        self.assertRaises(Exception, self.pipeline.get_issue_record, 1)

    @responses.activate
    def test_notify_triaging(self):
        """Test notify triaging."""
        responses.add(responses.POST, 'http://server/api/1/pipeline/517056/action',
                      json={'created_at': '2020-04-03T06:08:19.228497Z', 'kind': {'name': 'triaged'}})

        self.pipeline.notify_triaging()
        self.assertEqual('{"name": "triaged"}', responses.calls[0].request.body)

    @responses.activate
    def test_notify_triaging_dry_run(self):
        """Test notify triaging. Don't notify on dry run."""
        responses.add(responses.POST, 'http://server/api/1/pipeline/517056/action',
                      json={'created_at': '2020-04-03T06:08:19.228497Z', 'kind': {'name': 'triaged'}})

        self.pipeline.dry_run = True
        self.pipeline.notify_triaging()
        self.assertEqual(0, len(responses.calls))

    @responses.activate
    def test_unidentified_testruns(self):
        """Test unidentified_testruns contains the correct testruns."""
        responses.add(responses.GET, 'http://server/api/1/pipeline/517056/jobs/test/failures',
                      json=mock_responses.pipeline_517056_failed_tests)
        responses.add(responses.GET, 'http://server/api/1/pipeline/517056/issue/record',
                      json=mock_responses.pipeline_517056_issuerecords)

        # mock_responses.pipeline_517056_issuerecords contains 3 failed testruns:
        # - 1 without issue record (791256).
        # - 1 with unidentified issue record (791303).
        # - 1 with a correct issue record (791584).
        # pipeline.unidentified_testruns should return the first two only.

        # First check that these assumptions are ok.
        self.assertEqual(3, len(mock_responses.pipeline_517056_failed_tests['results']['jobs']))
        self.assertEqual(791303, mock_responses.pipeline_517056_failed_tests['results']['jobs'][0]['id'])
        self.assertEqual(791256, mock_responses.pipeline_517056_failed_tests['results']['jobs'][1]['id'])
        self.assertEqual(791584, mock_responses.pipeline_517056_failed_tests['results']['jobs'][2]['id'])
        self.assertEqual(2, len(mock_responses.pipeline_517056_issuerecords['results']['issue_records']))
        self.assertEqual(
            36, mock_responses.pipeline_517056_issuerecords['results']['issue_records'][0]['issue']['id']
        )
        self.assertEqual(
            791584, mock_responses.pipeline_517056_issuerecords['results']['issue_records'][0]['testruns'][0]['id']
        )
        self.assertEqual(
            57, mock_responses.pipeline_517056_issuerecords['results']['issue_records'][1]['issue']['id']
        )
        self.assertEqual(
            791303, mock_responses.pipeline_517056_issuerecords['results']['issue_records'][1]['testruns'][0]['id']
        )

        # Now test the method output.
        self.assertEqual(2, len(self.pipeline.unidentified_testruns))
        self.assertEqual(791303, self.pipeline.unidentified_testruns[0].id)
        self.assertEqual(791256, self.pipeline.unidentified_testruns[1].id)

    @responses.activate
    def test_unidentified_testruns_cached(self):
        """Test unidentified_testruns correctly caches the request."""
        responses.add(responses.GET, 'http://server/api/1/pipeline/517056/jobs/test/failures',
                      json=mock_responses.pipeline_517056_failed_tests)
        responses.add(responses.GET, 'http://server/api/1/pipeline/517056/issue/record',
                      json=mock_responses.pipeline_517056_issuerecords)

        self.assertEqual(2, len(self.pipeline.unidentified_testruns))

        responses.replace(responses.GET, 'http://server/api/1/pipeline/517056/jobs/test/failures', status=500)
        responses.replace(responses.GET, 'http://server/api/1/pipeline/517056/issue/record', status=500)

        self.assertEqual(2, len(self.pipeline.unidentified_testruns))

    @patch('run.TestFailureChecker')
    @patch('run.Pipeline.unidentified_testruns', Mock())
    def test_get_issues(self, testfailurechecker):
        """Test get_issues."""

        failures = [(1, [1])]
        mock_checker = Mock()
        mock_checker.return_value = failures
        mock_checker.unidentified_tests = Mock()
        mock_checker.unidentified_tests.return_value = [2, 3, 4]

        testfailurechecker.return_value = mock_checker
        issues = self.pipeline.get_issues()

        testfailurechecker.assert_called_with(self.pipeline.pipeline, self.pipeline.unidentified_testruns)
        mock_checker.unidentified_tests.assert_called_with(failures)

        self.assertEqual([(1, [1]), (57, [2, 3, 4])], issues)

    @patch('run.TestFailureChecker')
    @patch('run.Pipeline.unidentified_testruns', Mock())
    def test_get_issues_no_failures(self, testfailurechecker):
        """Test get_issues. No failures found."""

        failures = []
        mock_checker = Mock()
        mock_checker.return_value = failures
        mock_checker.unidentified_tests = Mock()

        testfailurechecker.return_value = mock_checker
        issues = self.pipeline.get_issues()

        testfailurechecker.assert_called_with(self.pipeline.pipeline, self.pipeline.unidentified_testruns)
        self.assertFalse(mock_checker.unidentified_tests.called)

        self.assertEqual([], issues)

    @patch('run.Pipeline.get_issues')
    @patch('run.Pipeline.notify_triaging')
    @patch('run.Pipeline.get_issue_record')
    def test_process_dry_run(self, get_issue_record, notify_triaging, get_issues):
        """Test process. Dry run."""
        get_issues.return_value = []
        self.pipeline.dry_run = True

        self.pipeline.process()

        self.assertTrue(get_issues.called)
        self.assertFalse(notify_triaging.called)
        self.assertFalse(get_issue_record.called)

    @patch('run.Pipeline.get_issues')
    @patch('run.Pipeline.notify_triaging')
    @patch('run.Pipeline.get_issue_record')
    def test_process_no_issues_found(self, get_issue_record, notify_triaging, get_issues):
        """Test process. Found no issues."""
        get_issues.return_value = []

        self.pipeline.process()

        self.assertTrue(get_issues.called)
        self.assertTrue(notify_triaging.called)
        self.assertFalse(get_issue_record.called)

    @patch('run.Pipeline.get_issues')
    @patch('run.Pipeline.notify_triaging')
    @patch('run.Pipeline.get_issue_record')
    def test_process(self, get_issue_record, notify_triaging, get_issues):
        """Test process."""
        @dataclass
        class Testrun:
            """Testrun Mock."""
            id: int  # pylint: disable=invalid-name

        get_issues.return_value = [(1, []), (2, [Testrun(1), Testrun(99)])]
        self.pipeline.process()
        self.assertTrue(get_issues.called)
        self.assertTrue(notify_triaging.called)

        get_issue_record.assert_has_calls([
            call(1),
            call().__bool__(),                  # if issue:
            ANY,                                # for testrun in issue.testruns
            call().edit(testrun_ids=[],
                        bot_generated=True),    # issue.edit
            call(2),
            call().__bool__(),                  # if issue:
            ANY,                                # for testrun in issue.testruns
            call().edit(testrun_ids=[1, 99],
                        bot_generated=True),    # issue.edit
            call(57),
            call().__bool__(),                  # if issue:
            call().delete(),                    # unidentified_issue.delete()
        ])
