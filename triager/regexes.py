"""Regexes to look for bugs 🐛🐛."""
import re

from datawarehouse import Datawarehouse

import settings


DATAWAREHOUSE = Datawarehouse(settings.DATAWAREHOUSE_URL, settings.DATAWAREHOUSE_TOKEN)


class RegexChecker:
    # pylint: disable=too-few-public-methods
    """RegexChecker."""

    def __init__(self):
        """Init."""
        self.lookups = []

    @staticmethod
    def _compile_lookups(lookups):
        """Compile the regexes."""
        compiled_lookups = []
        for lookup in lookups:
            for field_name in ('text_match', 'test_name_match', 'file_name_match'):
                field = getattr(lookup, field_name)
                compiled_field = re.compile(f'.*{field}.*', re.DOTALL) if field else None
                setattr(lookup, f'compiled_{field_name}', compiled_field)

            compiled_lookups.append(lookup)

        return compiled_lookups

    def download_lookups(self):
        """Download and compile regexes from Datawarehouse."""
        self.lookups = self._compile_lookups(
            DATAWAREHOUSE.issue_regex.list()
        )

    def search(self, text, log, testrun):
        """Search for regexes ocurrences on text."""
        for lookup in self.lookups:
            text_match = lookup.compiled_text_match
            test_name_match = lookup.compiled_test_name_match
            file_name_match = lookup.compiled_file_name_match

            if test_name_match and not test_name_match.match(testrun.test['name']):
                continue

            if file_name_match and not file_name_match.match(log['name']):
                continue

            if not text_match.match(text):
                continue

            # Fallback. Found it.
            return lookup.issue['id']

        return settings.NOT_FOUND
