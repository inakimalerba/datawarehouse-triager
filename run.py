#!/usr/bin/python3
"""Datawarehouse failure triager."""
import argparse
import itertools
import sys
from concurrent.futures import as_completed
from concurrent.futures import ThreadPoolExecutor
from functools import lru_cache

from cki_lib import messagequeue
from cki_lib.logger import get_logger
from datawarehouse import Datawarehouse

import settings
from triager.checkers import TestFailureChecker

LOGGER = get_logger('cki.triager')
DATAWAREHOUSE = Datawarehouse(settings.DATAWAREHOUSE_URL, settings.DATAWAREHOUSE_TOKEN)


class Pipeline:
    def __init__(self, pipeline_id, reprocessed=False, dry_run=False):
        self.pipeline_id = pipeline_id
        self.reprocessed = reprocessed
        self.dry_run = dry_run
        self.pipeline = DATAWAREHOUSE.pipeline.get(pipeline_id)

    def get_issue_record(self, issue_id):
        """Get issue record already present on the pipeline."""
        issue_records = self.pipeline.issuerecords.list(issue_id=issue_id)
        if len(issue_records) > 1:
            raise Exception(f"More than one Issue Record id {issue_id}. This shouldn't happen.")

        try:
            return issue_records[0]
        except IndexError:
            return None

    def notify_triaging(self):
        """Notify Datawarehouse of the triaging action."""
        if self.dry_run:
            return
        self.pipeline.actions.create(name='triaged')

    @property
    @lru_cache(maxsize=128)
    def unidentified_testruns(self):
        """
        Get testruns that need triaging.

        When the pipeline has no issue records, returns all testruns with failures.
        If Issue Records are pressent, only return those testruns that are not
        included on them.
        Unidentified Failures are treated as no issue record.
        """
        testruns = self.pipeline.failures.test.list()
        identified_testruns_ids = []

        # Get the testrun_id of the testruns with a Issue Record.
        issue_records = self.pipeline.issuerecords.list()
        for issue_record in issue_records:
            if issue_record.issue['id'] == settings.UNIDENTIFIED_FAILURE:
                continue
            identified_testruns_ids.extend(
                [testrun['id'] for testrun in issue_record.testruns]
            )

        # Remove those testruns from the list of test runs unidentified.
        testruns = [testrun for testrun in testruns if testrun.id not in identified_testruns_ids]

        return testruns

    def process(self):
        LOGGER.debug('%d - Processing.', self.pipeline_id)
        found_issues = self.get_issues()

        if self.dry_run:
            LOGGER.info('%d - Skipping. Dry run.', self.pipeline_id)
            return

        if not found_issues:
            if not self.reprocessed:
                self.notify_triaging()
            return

        for issue_id, affected_testruns in found_issues:
            LOGGER.info('%d - Creating report for issue %d', self.pipeline_id, issue_id)
            affected_testruns_ids = [testrun.id for testrun in affected_testruns]

            # Check if the pipeline has a record for this issue already.
            issue = self.get_issue_record(issue_id)

            if issue:
                # Append the new testruns to it.
                new_testrun_ids = [testrun.id for testrun in issue.testruns]
                new_testrun_ids.extend(affected_testruns_ids)
                issue.edit(testrun_ids=new_testrun_ids, bot_generated=True)
            else:
                # Create a new record.
                self.pipeline.issuerecords.create(issue_id=issue_id, testrun_ids=affected_testruns_ids,
                                                  bot_generated=True)

        if not any([issue_id == settings.UNIDENTIFIED_FAILURE for issue_id, _ in found_issues]):
            # No unidentified issues in the results.
            # Check if there's an unidentified issue on the pipeline and delete it.
            unidentified_issue = self.get_issue_record(settings.UNIDENTIFIED_FAILURE)
            if unidentified_issue:
                unidentified_issue.delete()

        self.notify_triaging()

    def get_issues(self):
        """Run the checks and return the issues."""
        LOGGER.debug('%d - Getting issues.', self.pipeline_id)
        test_failure_checker = TestFailureChecker(self.pipeline, self.unidentified_testruns)
        failures = test_failure_checker()

        if failures:
            LOGGER.debug('%d - Checking for unidentified issues.', self.pipeline_id)
            unidentified_tests = test_failure_checker.unidentified_tests(failures)
            if unidentified_tests:
                LOGGER.debug('%d - Unidentified issues found.', self.pipeline_id)
                failures.append((settings.UNIDENTIFIED_FAILURE, unidentified_tests))

        LOGGER.debug('%d - Found %d failures.', self.pipeline_id, len(failures))
        return failures

    @classmethod
    def triage(cls, pipeline_id, reprocessed=False, dry_run=False):
        """Triage pipeline directly."""
        pipeline = cls(pipeline_id, reprocessed, dry_run)
        pipeline.process()


def callback(dry_run, body):
    """Handle a single message."""
    if body['status'] == 'finished':
        pipeline = Pipeline(body['pipeline_id'], dry_run=dry_run)
        pipeline.process()
    else:
        LOGGER.debug(f'Skipping pipeline {body["pipeline_id"]}: {body["status"]}')

    if dry_run:
        raise Exception('Not acking message because of dry run')


def triage_queue(dry_run=False):
    """Triage elements received using the message queue."""
    LOGGER.info("Running checks on queue items.")
    queue = messagequeue.MessageQueue(
        settings.RABBITMQ_HOST,
        settings.RABBITMQ_PORT,
        settings.RABBITMQ_USER,
        settings.RABBITMQ_PASSWORD,
    )
    queue.consume_messages(settings.EXCHANGE_NAME, ['#'],
                           lambda routing_key, body: callback(dry_run, body),
                           queue_name=settings.MESSAGE_QUEUE_NAME,
                           inactivity_timeout=settings.RABBITMQ_TIMEOUT_S)


def get_pipelines(kind):
    params = {}
    if kind == 'unidentified':
        endpoint = DATAWAREHOUSE.issue.get(settings.UNIDENTIFIED_FAILURE).record
    elif kind == 'untriaged':
        endpoint = DATAWAREHOUSE.failed_pipelines
        params.update({'stage': 'test'})
    else:
        raise Exception(f'kind {kind} of pipelines is not supported.')

    page = 0
    page_size = 30
    while True:
        params.update({'limit': page_size, 'offset': page*page_size})
        records = endpoint.list(**params)

        if not records:
            # Reached the end.
            return

        page += 1
        for record in records:
            yield record.pipeline_id


def triage_api(kind, max_pipelines, max_workers, dry_run=False):
    """Triage pipelines by listing them usings the API according to {kind}."""
    LOGGER.info(f"Running checks on '{kind}' pipelines.")

    with ThreadPoolExecutor(max_workers=max_workers) as executor:
        future_to_pipeline_id = {
            executor.submit(Pipeline.triage, pipeline_id, reprocessed=True, dry_run=dry_run): pipeline_id
            for pipeline_id in itertools.islice(get_pipelines(kind), max_pipelines)
        }

        failed = 0
        for future in as_completed(future_to_pipeline_id):
            pipeline_id = future_to_pipeline_id[future]
            try:
                future.result()
            except Exception:
                LOGGER.exception('%d: Failed while processing.', pipeline_id)
                failed += 1
        sys.exit(failed)


def parse_args():
    """Parse arguments."""
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument('--dry-run', action='store_true',
                        help='Just look for issues. Do not create failure reports.')
    parser.add_argument('--check', choices=['untriaged', 'unidentified', 'queue', 'single'], default='queue',
                        help=('Decide what to check. Reprocess pipelines without issue records, '
                              'unidentified issues or monitor queue. Single mode expects --pipeline-id.'))
    parser.add_argument('--max-pipelines', type=int, default=200,
                        help=('The maximum number of pipelines to check while iterating on the API '
                              '(not used in queue mode)'))
    parser.add_argument('--max-workers', type=int, default=5,
                        help=('The maximum number of pipelines to process in parallel '
                              '(not used in queue mode)'))
    parser.add_argument('--pipeline-id', type=int,
                        help='Pipeline id of the pipeline to triage (only used in single mode).')
    parser.add_argument('--reprocessed', action='store_true',
                        help='Set reprocessed flag (only used in single mode).')

    return parser.parse_args()


if __name__ == '__main__':
    arguments = parse_args()

    if arguments.check == 'queue':
        triage_queue(arguments.dry_run)
    elif arguments.check == 'single':
        if not arguments.pipeline_id:
            LOGGER.error('--pipeline-id is expected to run single check.')
            sys.exit(1)
        Pipeline(arguments.pipeline_id, reprocessed=arguments.reprocessed, dry_run=arguments.dry_run).process()
    else:
        triage_api(arguments.check, arguments.max_pipelines, arguments.max_workers, arguments.dry_run)
